package org.minbox.framework.config;

/**
 * Classes that implement this interface automatically register bean instances
 * through {@link org.springframework.beans.factory.support.BeanDefinitionRegistry}
 *
 * @author 恒宇少年
 * @see org.springframework.context.annotation.ClassPathBeanDefinitionScanner
 */
public interface RegisterBean {
}
